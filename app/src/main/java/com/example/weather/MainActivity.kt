package com.example.weather

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.AsyncTask
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import org.json.JSONObject
import java.lang.Exception
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    val CITY: String = "lyubertsy,ru"
    val API: String = "77abd220c609896e8bf62182adab8ae0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        weatherTask().execute()
    }

    /*fun GetUserLocation() {
        var mylocation = locationListener()
        var locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,1f,mylocation)
    }

    inner class locationListener: LocationListener {
        constructor():super() {
            mylocation = Location("me")
            mylocation!!.longitude
            mylocation!!.latitude
        }
        override fun onLocationChanged(location: Location) {
            mylocation = location
        }

        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }*/

    inner class weatherTask() : AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
            findViewById<ProgressBar>(R.id.loader).visibility = View.VISIBLE
            findViewById<RelativeLayout>(R.id.mainContainer).visibility = View.GONE
            findViewById<TextView>(R.id.errortext).visibility = View.GONE
        }

        override fun doInBackground(vararg p0: String?): String? {
            var response: String?
            try {
                response = URL("https://api.openweathermap.org/data/2.5/weather?q=$CITY&units=metric&appid=$API&lang=ru")
                    .readText(Charsets.UTF_8)
            }
            catch ( e: Exception)
            {
                response = null
            }
            return response
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                val jsonObj = JSONObject(result)
                val main = jsonObj.getJSONObject("main")
                val sys = jsonObj.getJSONObject("sys")
                val wind = jsonObj.getJSONObject("wind")
                val weather = jsonObj.getJSONArray("weather").getJSONObject(0)
                val updatedAt:Long = jsonObj.getLong("dt")
                val updatedAtText = SimpleDateFormat( "dd,MMM HH:mm", Locale("ru", "RU")).format(Date(updatedAt*1000))
                val temp = main.getInt("temp").toString()+ "°C"
                val tempMin = "Min temp: "+main.getInt("temp_min").toString()+"°C"
                val tempMax = "Max temp: "+main.getInt("temp_max").toString()+ "°C"
                val pressure = main.getString("pressure")
                val humidity = main.getString("humidity")
                val sunrise : Long = sys.getLong("sunrise")
                val sunset : Long = sys.getLong("sunset")
                val windSpeed = wind.getString("speed")
                val weatherDescription = weather.getString("description")
                val address = jsonObj.getString("name")+", "+sys.getString("country")


                val icon =  weather.getString("icon")
                if (icon == "01d") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.sun))
                if (icon == "01n") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.moon))
                if (icon == "02d") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.sun_clouds))
                if (icon == "02n") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.moon_clouds))
                if (icon == "03d" || icon == "03n") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.clouds))
                if (icon == "04d" || icon == "04n") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.hard_clouds))
                if (icon == "09d" || icon == "09n") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.hard_rain))
                if (icon == "10d") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.rain))
                if (icon == "10n") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.night_rain))
                if (icon == "11d" || icon == "11n") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.storm))
                if (icon == "13d" || icon == "13n") findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.snow))
                if (icon == "50d" || icon == "50n") {findViewById<ImageView>(R.id.mainIconContainer).setImageDrawable(getDrawable(R.drawable.mist))}


                findViewById<TextView>(R.id.address).text = address
                findViewById<TextView>(R.id.updated_at).text = updatedAtText
                findViewById<TextView>(R.id.status).text = weatherDescription.capitalize()
                findViewById<TextView>(R.id.temp).text = temp
                findViewById<TextView>(R.id.temp_min).text = tempMin
                findViewById<TextView>(R.id.temp_max).text = tempMax
                findViewById<TextView>(R.id.sunrise).text = SimpleDateFormat( "dd,MMM HH:mm", Locale("ru", "RU")).format(Date(updatedAt*1000))
                findViewById<TextView>(R.id.sunset).text = SimpleDateFormat( "dd,MMM HH:mm", Locale("ru", "RU")).format(Date(updatedAt*1000))
                findViewById<TextView>(R.id.wind).text = windSpeed
                findViewById<TextView>(R.id.pressure).text = pressure
                findViewById<TextView>(R.id.humidity).text = humidity

                findViewById<ProgressBar>(R.id.loader).visibility = View.GONE
                findViewById<RelativeLayout>(R.id.mainContainer).visibility = View.VISIBLE
            }
            catch (e : Exception)
            {
                findViewById<ProgressBar>(R.id.loader).visibility = View.GONE
                findViewById<TextView>(R.id.errortext).visibility = View.VISIBLE
            }
        }
    }
}